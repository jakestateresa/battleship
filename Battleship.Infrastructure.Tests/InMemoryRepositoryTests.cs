using System;
using System.Collections.Generic;
using Xunit;

namespace Battleship.Infrastructure.Tests
{
    class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
    }
    
    public class InMemoryRepositoryTests
    {
        private InMemoryRepository<int, Player> repository;
        private Player player;

        public InMemoryRepositoryTests()
        {
            repository = new InMemoryRepository<int, Player>((p) => p.Id);

            player = new Player
            {
                Id = new Random(DateTime.Now.Millisecond).Next(1000, 9999),
                Name = "Ready Player One",
                Score = 10
            };
        }
                
        [Fact]
        public void AddTests()
        {
            repository.Add(player);

            Assert.True(repository.Exists(player));
        }

        [Fact]
        public void AddFailsWhenItemExistTests()
        {
            repository.Add(player);
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.Add(player));                  
        }

        [Fact]
        public void EditTests()
        {
            repository.Add(player);

            player.Score++;

            repository.Edit(player);

            Assert.Equal(11, player.Score);
        }

        [Fact]
        public void EditFailsWhenItemDoesNotExistTests()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.Edit(player));
        }

        [Fact]
        public void DeleteTests()
        {
            repository.Add(player);
            repository.Delete(player);

            Assert.False(repository.Exists(player));
        }

        [Fact]
        public void DeleteFailsWhenItemDoesNotExistTests()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => repository.Delete(player));
        }

        [Fact]
        public void GetTests()
        {
            repository.Add(player);
            var foundProduct = repository.Get(player.Id);

            Assert.Equal(player, foundProduct);

            var notFoundBoard = repository.Get(123);

            Assert.Null(notFoundBoard);
        }

        [Fact]
        public void GetAllTests()
        {
            repository.Add(player);
            var foundPlayers = repository.GetAll();

            Assert.NotEmpty(foundPlayers);
            Assert.Equal(player, new List<Player>(foundPlayers)[0]);
        }
    }
}