Battleship
============

Assumptions
------------
* .Net Core 2.2 is installed on your machine
* The original game rules does not allow battleships to overlap in the board. This was not included in the specification but was implemented anyway for consistency with game rules.
* The original game rules does not allow battleships the same point to be attacked in the board. This was not included in the specification. The implementation does not battleships to take further damage when attack is called passing the same location. Further to this, attack returns false when a battleship does not take any damage.

Overview
------------

The application is implemented using .Net 2.2 and structured using DDD.

XUnit tests are available to assert the following game rules:

* EnsureBattleshipCannotOverlapBoard
* EnsureBattleshipCannotOverlapOtherBattleshipsHorizontally
* EnsureBattleshipCannotOverlapOtherBattleshipsVertically
* EnsureBoardInitialisedCorrectly
* VerifyAttackHitTestHorizontally
* VerifyAttackHitTestVertically
* VerifyShipDoesNotTakeFurtherDamageWhenItTakesMultipleHitsOnTheSameSquare
* VerifyShipSinksWhenOnAllOccupiedSquaresAreHit

The source code is available in Bitbucket and can be accessed via the following url:

* https://bitbucket.org/jakestateresa/battleship

The application is deployed in an Azure App Service and can be accessed via the following url:

* https://battleship-www.azurewebsites.net/swagger/index.html

Note: A postman collection and associated environment file is provided under the ```./Postman``` folder.