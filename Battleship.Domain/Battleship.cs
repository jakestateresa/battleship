using System.Collections.Generic;
using Battleship.Domain.ValueObjects;

namespace Battleship.Domain
{
    public class Battleship
    {
        private List<string> hits;
        public Battleship(Size size)
        {
            Size = size;
            Lives = size.Width * size.Height;
            hits = new List<string>();
        }
        
        public Size Size { get; }

        public int Lives { get; private set; }

        public bool IsSunk => Lives == 0;
        
        public bool TakeDamage(int x, int y)
        {
            var hit = $"{x}{y}";
            if (hits.Contains(hit))
            {
                return false;
            }
            
            hits.Add(hit);
            Lives--;
            return true;
        }
    }
}