﻿using System;
using System.Collections.Generic;
using Battleship.Domain.ValueObjects;

namespace Battleship.Domain
{
    public class Board
    {
        private Battleship[,] tiles;

        public Board(string id, Size size)
        {
            Id = id;
            Size = size;
            Battleships = new List<Battleship>();
            tiles = new Battleship[size.Width, size.Height];
        }

        public string Id { get; }
        
        public Size Size { get; }
        
        public List<Battleship> Battleships { get; }

        public void AddBattleship(Battleship battleship, Point point)
        {
            if (point.X < 0 || point.X > Size.Width - 1 ||
                point.Y < 0 || point.Y > Size.Height - 1 ||
                point.X + (battleship.Size.Width -1) > Size.Width - 1 ||
                point.Y + (battleship.Size.Height - 1)> Size.Height -1)
            {
                throw new ArgumentOutOfRangeException(nameof(point), $"Unable to add battleship at point ({point.X}, {point.Y}). Battleships must fit entirely on the board.");
            }

            //layout the battleship on the tiles
            for (int x = point.X; x < point.X + battleship.Size.Width; x++)
            {
                for (int y = point.Y; y < point.Y + battleship.Size.Height; y++)
                {
                    //perform a hit test to ensure battleship
                    //does not overlap other battleships
                    if (HitTest(new Point(x, y)))
                    {
                        throw new ArgumentOutOfRangeException(nameof(battleship), $"Unable to add battleship at point ({point.X}, {point.Y}). Battleships must not overlap with other battleships.");
                    }
                    
                    tiles[x, y] = battleship;
                }
            }
            Battleships.Add(battleship);
        }

        public bool Attack(Point point)
        {
            if (HitTest(point))
            {
                var battleship = tiles[point.X, point.Y];
                var damageTaken = battleship.TakeDamage(point.X, point.Y);
                
                return damageTaken;
            }

            return false;
        }
        
        private bool HitTest(Point point)
        {
            return HitTest(point.X, point.Y);
        }
        
        private bool HitTest(int x, int y)
        {
            return tiles[x, y] != null;
        }
    }
}