using System;
using Battleship.Domain.ValueObjects;
using Xunit;

namespace Battleship.Domain.Tests
{
    public class BoardTests
    {
        private Board board;
        private Battleship battleship;
        public BoardTests()
        {
            board = new Board(Guid.NewGuid().ToString(),  new Size(10, 10));
            battleship = new Battleship(new Size(3, 4));
        }

        [Fact]
        public void EnsureBoardInitialisedCorrectly()
        {
            Assert.Equal(board.Size, new Size(10, 10));
            Assert.Empty(board.Battleships);
        }
        
        [Fact]
        public void EnsureBattleshipCannotOverlapBoard()
        {
            var exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(-1, 0));
            });
            Assert.Equal("Unable to add battleship at point (-1, 0). Battleships must fit entirely on the board.\nParameter name: point", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(10, 0));
            });
            Assert.Equal("Unable to add battleship at point (10, 0). Battleships must fit entirely on the board.\nParameter name: point", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(0, -1));
            });
            Assert.Equal("Unable to add battleship at point (0, -1). Battleships must fit entirely on the board.\nParameter name: point", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(0, 10));
            });
            Assert.Equal("Unable to add battleship at point (0, 10). Battleships must fit entirely on the board.\nParameter name: point", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(8, 0));
            });
            Assert.Equal("Unable to add battleship at point (8, 0). Battleships must fit entirely on the board.\nParameter name: point", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(0, 7));
            });
            Assert.Equal("Unable to add battleship at point (0, 7). Battleships must fit entirely on the board.\nParameter name: point", exception.Message);

            board.AddBattleship(battleship, new Point(0, 0));
        }
        
        [Fact]
        public void EnsureBattleshipCannotOverlapOtherBattleshipsHorizontally()
        {
            board.AddBattleship(battleship, new Point(4, 5));

            //can add item adjacent to the left of battleship
            board.AddBattleship(battleship, new Point(1, 5));

            var exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(2, 5));
            });
            Assert.Equal("Unable to add battleship at point (2, 5). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(3, 5));
            });
            Assert.Equal("Unable to add battleship at point (3, 5). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(4, 5));
            });
            Assert.Equal("Unable to add battleship at point (4, 5). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);
            
            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(5, 5));
            });
            Assert.Equal("Unable to add battleship at point (5, 5). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(6, 5));
            });
            Assert.Equal("Unable to add battleship at point (6, 5). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);

            //can add item adjacent to the right of battleship
            board.AddBattleship(battleship, new Point(7, 5));
        }
        
        [Fact]
        public void EnsureBattleshipCannotOverlapOtherBattleshipsVertically()
        {
            var battleship = new Battleship(new Size(4, 3));
            board.AddBattleship(battleship, new Point(5, 4));

            //can add item immediately to the top of battleship
            board.AddBattleship(battleship, new Point(5, 1));

            var exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(5, 2));
            });
            Assert.Equal("Unable to add battleship at point (5, 2). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(5, 3));
            });
            Assert.Equal("Unable to add battleship at point (5, 3). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(5, 4));
            });
            Assert.Equal("Unable to add battleship at point (5, 4). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);
  
            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(5, 5));
            });
            Assert.Equal("Unable to add battleship at point (5, 5). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);

            exception = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                board.AddBattleship(battleship, new Point(5, 6));
            });
            Assert.Equal("Unable to add battleship at point (5, 6). Battleships must not overlap with other battleships.\nParameter name: battleship", exception.Message);

            //can add item immediately to the bottom of battleship
            board.AddBattleship(battleship, new Point(5, 7));
        }
        
        [Fact]
        public void VerifyAttackHitTestHorizontally()
        {
            board.AddBattleship(battleship, new Point(5, 4));

            //attacking to the immediate left does not hit the battleship
            var isHit = board.Attack(new Point(4, 4));
            Assert.False(isHit);
            
            isHit = board.Attack(new Point(5, 4));
            Assert.True(isHit);
            
            isHit = board.Attack(new Point(6, 4));
            Assert.True(isHit);
            
            isHit = board.Attack(new Point(7, 4));
            Assert.True(isHit);
            
            //attacking to the immediate right does not hit the battleship
            isHit = board.Attack(new Point(8, 4));
            Assert.False(isHit);
        }
        
        [Fact]
        public void VerifyAttackHitTestVertically()
        {
            var battleship = new Battleship(new Size(4, 3));
            board.AddBattleship(battleship, new Point(4, 5));

            //attacking to the immediate top does not hit the battleship
            var isHit = board.Attack(new Point(4, 4));
            Assert.False(isHit);

            isHit = board.Attack(new Point(4, 5));
            Assert.True(isHit);

            isHit = board.Attack(new Point(4, 6));
            Assert.True(isHit);

            isHit = board.Attack(new Point(4, 7));
            Assert.True(isHit);

            //attacking to the immediate bottom does not hit the battleship
            isHit = board.Attack(new Point(4, 8));
            Assert.False(isHit);
        }
        
        [Fact]
        public void VerifyShipDoesNotTakeFurtherDamageWhenItTakesMultipleHitsOnTheSameSquare()
        {
            board.AddBattleship(battleship, new Point(5, 4));
            
            Assert.Equal(battleship.Size.Width * battleship.Size.Height, battleship.Lives);
            
            var isHit = board.Attack(new Point(5, 4));
            Assert.True(isHit);
            Assert.Equal((battleship.Size.Width * battleship.Size.Height) - 1, battleship.Lives);

            //Battleship does not take duplicate damages on the same square
            isHit = board.Attack(new Point(5, 4));
            Assert.False(isHit);
            Assert.Equal((battleship.Size.Width * battleship.Size.Height) - 1, battleship.Lives);
        }
        
        [Fact]
        public void VerifyShipSinksWhenOnAllOccupiedSquaresAreHit()
        {
            var battleship = new Battleship(new Size(2, 2));
            board.AddBattleship(battleship, new Point(0, 0));
                        
            Assert.Equal(battleship.Size.Width * battleship.Size.Height, battleship.Lives);
            Assert.False(battleship.IsSunk);
            
            var isHit = board.Attack(new Point(0, 0));
            Assert.True(isHit);
            Assert.Equal((battleship.Size.Width * battleship.Size.Height) - 1, battleship.Lives);
            Assert.False(battleship.IsSunk);

            isHit = board.Attack(new Point(0, 1));
            Assert.True(isHit);
            Assert.Equal((battleship.Size.Width * battleship.Size.Height) - 2, battleship.Lives);
            Assert.False(battleship.IsSunk);

            isHit = board.Attack(new Point(1, 1));
            Assert.True(isHit);
            Assert.Equal((battleship.Size.Width * battleship.Size.Height) - 3, battleship.Lives);
            Assert.False(battleship.IsSunk);

            isHit = board.Attack(new Point(1, 0));
            Assert.True(isHit);
            Assert.Equal((battleship.Size.Width * battleship.Size.Height) - 4, battleship.Lives);
            Assert.True(battleship.IsSunk); //no lives, battleship is sunk
        }
    }
}