using System;
using System.Collections.Generic;
using Battleship.Application.Interfaces;

namespace Battleship.Infrastructure
{
    public class InMemoryRepository<TKey, TItem> : IRepository<TKey, TItem>
        where TItem : class
        where TKey : IEquatable<TKey>
    {
        private Func<TItem, TKey> getId;
        private List<TItem> items = new List<TItem>();

        public InMemoryRepository(Func<TItem, TKey> getId)
        {
            this.getId = getId;
        }

        public void Add(TItem item)
        {
            if (Exists(item))
            {
                throw new ArgumentOutOfRangeException(nameof(item));
            }

            items.Add(item);
        }

        public void Edit(TItem item)
        {
            if (!Exists(item))
            {
                throw new ArgumentOutOfRangeException(nameof(item));
            }

            var index = items.IndexOf(item);
            items[index] = item;
        }

        public void Delete(TItem item)
        {
            if (!Exists(item))
            {
                throw new ArgumentOutOfRangeException(nameof(item));
            }

            items.Remove(item);
        }

        public TItem Get(TKey key) => items.Find(currentItem => getId(currentItem).Equals(key));

        public IEnumerable<TItem> GetAll() => items.AsReadOnly();

        public bool Exists(TItem item) => Get(getId(item)) != default(TItem);
        
        public bool Exists(TKey key) => Get(key) != default(TItem);
    }
}