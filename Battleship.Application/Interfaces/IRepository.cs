using System;
using System.Collections.Generic;

namespace Battleship.Application.Interfaces
{
    public interface IRepository<in TKey, TItem>
        where TItem : class
        where TKey : IEquatable<TKey>
    {
        void Add(TItem item);

        void Edit(TItem item);

        void Delete(TItem item);

        TItem Get(TKey key);

        IEnumerable<TItem> GetAll();

        bool Exists(TItem item);
        
        bool Exists(TKey key);
    }
}
