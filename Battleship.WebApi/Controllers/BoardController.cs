using System;
using Battleship.Domain;
using Battleship.Domain.ValueObjects;
using Battleship.WebApi.Dto;
using Microsoft.AspNetCore.Mvc;

namespace Battleship.WebApi.Controllers
{
    [Route("api/board")]
    [ApiController]
    public class BoardController : Controller
    {
        private static Board board;
        
        /// <summary>
        /// Creates a new board. All existing data on the old board will be cleared
        /// </summary>
        /// <returns>The newly created board</returns>
        [HttpPost]
        public IActionResult Create()
        {
            board = new Board(Guid.NewGuid().ToString(),  new Size(10, 10));
            return Ok(board);
        }
        
        /// <summary>
        /// Returns the current state of the board.
        /// </summary>
        /// <returns>A board</returns>
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            if (board.Id != id)
            {
                return NotFound();
            }

            return Ok(board);
        }
        
        /// <summary>
        /// Adds a battleship to the board.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///        {
        ///            "battleship": {
        ///                "size": {
        ///                    "width": 4,
        ///                    "height": 5
        ///                }
        ///            },
        ///            "point": {
        ///                "x": 5, 
        ///                "y": 5
        ///            }
        ///        }
        ///
        /// </remarks>
        /// <param name="request"></param>
        /// <response code="200">The battleship is successfully added to the board</response>
        /// <response code="400">The battleship cannot be added to the board. It can be because the battleship overlaps the board or with other battleships in the board</response>   
        [HttpPost("{id}/battleships/")]
        public IActionResult AddBattleship(string id, [FromBody] CreateBattleshipRequest request)
        {
            if (board.Id != id)
            {
                return NotFound();
            }

            try
            {
                board.AddBattleship(request.Battleship, request.Point);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok();
        }
        
        /// <summary>
        /// Performs an attack at a point in the board.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///        {
        ///           "x": 5, 
        ///           "y": 5
        ///        }
        ///
        /// </remarks>
        /// <param name="request"></param>
        [HttpPost("{id}/attack")]
        public ActionResult<bool> Attack(string id, [FromBody] Point point)
        {
            if (board.Id != id)
            {
                return NotFound();
            }

            var isHit = board.Attack(point); 
            return Ok(isHit);
        }
    }
}