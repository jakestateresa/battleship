using Battleship.Domain.ValueObjects;

namespace Battleship.WebApi.Dto
{
    public class CreateBattleshipRequest
    {
        public Battleship.Domain.Battleship Battleship { get; set; }
        public Point Point { get; set; }
    }
}